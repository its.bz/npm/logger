const
    fs = require('fs'),
    morgan = require('morgan'),
    winston = require('winston'),
    util = require("util"),
    path = require("path"),
    chalk = require('chalk')

require('winston-logrotate')

/**
 * Creates log directory and empty files if not exists
 */
const create_log_files = (log_files) => {
    Object.keys(log_files).map(file => {
        const filename = log_files[file]
        const dir = path.dirname(filename)
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir, {recursive: true})
            fs.writeFileSync(filename, '')
        }
    })
}

/**
 * Access logger object
 * @param express_app
 * @param log_file
 */
const create_access_log = (express_app, log_file) => {
    const accessLogStream = fs.createWriteStream(log_file, {flags: 'w'})
    morgan.token('body', function (req) {
        return JSON.stringify(req.body)
    });
    express_app.use(morgan('dev'))
    express_app.use(morgan(":method :url @:date\n :remote-addr :status :response-time ms\n :body", {stream: accessLogStream}))
}

/**
 * Centralized logger object
 */
const create_log_wrapper = ({app_name, app_version, info_log, error_log, bot, chat}) => {
    const log_format_console = winston.format.printf(({timestamp, level, message, meta}) => {
        level = level.toUpperCase()
        timestamp = new Date(timestamp).toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '')

        const colors = {
            'DEBUG': 'white',
            'INFO': 'green',
            'WARN': 'yellow',
            'ERROR': 'red'
        }
        const color = colors[level] || 'white'
        return chalk[color](`[${level}]`) + ` ${timestamp}: ${message} ${meta ? '\n' + JSON.stringify(meta) : ''}`
    })

    const log_format_file = winston.format.printf(({timestamp, level, message, meta}) => {
        level = level.toUpperCase()
        timestamp = new Date(timestamp).toISOString()
            .replace(/T/, ' ')
            .replace(/\..+/, '')
        return `[${level}] ${timestamp}: ${message} ${meta ? '\n' + JSON.stringify(meta) : ''}`
    })

    let logger = winston.createLogger({
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.splat(),
            log_format_console
        ),
        transports: [
            new (winston.transports.Console)({
                colorize: true,
                handleExceptions: true,
                exitOnError: false,
                level: 'debug',
                format: log_format_console
            }),
            new (winston.transports.File)({
                filename: info_log,
                colorize: false,
                handleExceptions: true,
                exitOnError: false,
                level: 'debug',
                format: log_format_file,
                size: '100k',
                keep: 2,
                compress: true,
                file: info_log
            }),
            new (winston.transports.File)({
                filename: error_log,
                colorize: false,
                handleExceptions: true,
                exitOnError: false,
                level: 'error',
                format: log_format_file,
                size: '100k',
                keep: 5,
                compress: true,
                file: error_log,
            })
        ],
        exitOnError: false, // do not exit on handled exceptions
    });
    console.log = (...args) => logger.info.call(logger, ...args)
    console.info = (...args) => logger.info.call(logger, ...args)
    console.warn = (...args) => logger.warn.call(logger, ...args)
    console.debug = (...args) => logger.debug.call(logger, ...args)
    console.error = async (...args) => {
        let message
        const title = `#${app_name.replace(/[- ]/g, '_')} ${app_version}\n\n`
        if (args[0] && args[0].message && args[0].stack) {
            args = args[0]
            logger.error.call(logger, args.stack)
            message = `${args.stack}`
        } else {
            logger.error.call(logger, args)
            message = `${args.toString()}`
        }

        message = message.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;')
        message = `${title}<code>${message}</code>`
        if (bot)
            await bot.telegram.sendMessage(chat, message, {parse_mode: "HTML"})
                .catch(e => {
                    logger.error.call(logger, e)
                })
    }
}

/**
 * Catches global process exception
 */
const global_exception_wrapper = (log_file) => {
    const log_file_err = fs.createWriteStream(log_file, {flags: 'a'})
    process.on('uncaughtException', function (e) {
        console.error(e)
        log_file_err.write(util.format(e.stack) + '\n')
    })
}

const logger = (app, {
    app_name = 'app',
    app_version = '1.0.0',
    access_log = 'logs/access.log',
    info_log = 'logs/info.log',
    error_log = 'logs/error.log',
    fatal = 'logs/fatal.log',
    bot = undefined,
    chat = ''
}) => {
    create_log_files({access_log, info_log, error_log, fatal})
    create_access_log(app, access_log)
    create_log_wrapper({app_name, app_version, info_log, error_log, bot, chat})
    global_exception_wrapper(fatal)
}

module.exports = logger
